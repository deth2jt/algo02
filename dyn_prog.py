def lis(array, index, length, prev_val):
    if(index == length ):
        return 0

    contin = 0
    #print("i: ", i , "n: ", n)
    if(array[index ] > prev_val):
        prev_val = array[index ]
        contin = 1 + lis(array, index + 1, length, prev_val)


    return max(lis(array, index + 1, length, prev_val), contin )
    #return contin


def lovethyneighbour(lst):
    summ = []
    if(len(lst) == 0):
        return 0
    elif(len(lst) == 1):
        return lst[0]
    elif(len(lst) == 2):
        return max(lst[0], lst[1])
    else:
        return  max(lst[0] + lovethyneighbour(lst[2:])  ,     lovethyneighbour(lst[1:  ])  )
        #index += 3

def aroundtheblock(lst):
    summ = []
    if(len(lst) == 0):
        return 0
    elif(len(lst) == 1):
        return lst[0]
    elif(len(lst) == 2):
        return max(lst[0], lst[1])
    else:
        return  max(lst[0] + lst[2]  ,   lst[0] +  lst[1  ]  , lst[2] + lst[1]    )   + aroundtheblock(lst[3:] )


def mvblm(lst, m):
    if(len(lst) <= m ):
        return sum (lst)
    
    else:
        array = []
        #print("working with:", lst[0:m+1])
        for index in range(m+1):
            if (index == 0):
                #array += [sum (lst [1:m+1] )]
                value = sum (lst [1:m+1] )
                array += [ value  + mvblm(lst[m+2:], m)  ]
            elif(index == m):
                #array += [sum (lst [:m]) ]
                value = sum (lst [:m])
                array +=  [value  + mvblm(lst[m+1:], m)  ]
            else:
                #array += [sum (lst [0:index])  + sum(lst [index+1:m+1]) ]
                value = sum (lst [0:index])  + sum(lst [index+1:m+1])
                array +=  [value  + mvblm(lst[m+1:], m)  ]
        #print("array",array)
        return max(array)
        '''
        print("array",array)
        print("max(array[:m] )",max(array[:m] ))
        print("array[m] ",array[m] )

        
        skipIndex = array[m]   + mvblm(lst[m+1:], m)
        dontSkip = max(array[:m] )  + mvblm(lst[m+2:], m)
        val = max (skipIndex,dontSkip )
        print("val",val)
        return val
        '''
        #return max( array[m]   + mvblm(lst[m+1:], m)  , max(array[:m] )  + mvblm(lst[m+2:], m) )
        #return array[m]   + mvblm(lst[m+1:], m) 

        #return max( max(array )  , mvblm(lst[m+2:], m) )

def mvblm2(lst, m):
    if(len(lst) <= m ):
        return (lst,sum (lst))
    
    else:
        array = []
        #print("working with:", lst[0:m+1])
        for index in range(m+1):
            if (index == 0):
                #array += [sum (lst [1:m+1] )]
                value = sum (lst [1:m+1] )
                array += [    add (lst [1:m+1], value,   mvblm2(lst[m+2:], m))   ]
            elif(index == m):
                #array += [sum (lst [:m]) ]
                value = sum (lst [:m])
                array +=  [  add (lst [:m] ,value,  mvblm2(lst[m+1:], m))  ]
            else:
                #array += [sum (lst [0:index])  + sum(lst [index+1:m+1]) ]
                value = sum (lst [0:index])  + sum(lst [index+1:m+1])
                array +=  [   add (lst [0:index] + lst [index+1:m+1],value, mvblm2(lst[m+1:], m))  ]
        #print("array",array)
        return maxValue(array)

def add(lst, value, tuple1):
    return (lst + tuple1[0], value + tuple1[1])
def maxValue(array):
    a=-1
    targetIndex = 0
    for index in range(len(array)):
        #print("arrayarrayarray",array) 
        if array[index][1] > a:
            a = array[index][1]
            targetIndex = index
    return array[targetIndex] 


def lcs(array1, array2):
    if(len(array1) == 0 or len(array2) == 0):
        return 0
    elif(array1[0]==array2[0]):
        return 1 + lcs(array1[1:], array2[1:])
    else:
        return max(lcs(array1[1:], array2), lcs(array1, array2[1:]) )          


# Program for Longest Increasing Subsequence
if __name__ == '__main__':
 
    #array = [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
    #array = [10 ,22 ,9 , 33 , 21 , 50 , 41 , 60]
    #array = [5,2,7,8,1,3]
    
    array = [10,100,300,400,50, 4500,200,30,90]
    #array = [100,300,400,50]
    #array = [10,20,5,0,30]
    letters = "mangoes"
    letters1 = "mementos"
    #letters2 = "mnos"
    print("Original Array is", array)
    print("Length of LIS is", lis(array, 0, len(array), float('-inf')))
    print("Length of lovethyneighbour is", lovethyneighbour(array))
    print("Length of aroundtheblock is", aroundtheblock(array))
    print("Length of lcs is", lcs(letters, letters1))
    print("Length of mvblm is", mvblm2(array, 2))
